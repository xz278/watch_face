package xz.watchface01;

import android.app.Activity;
import android.app.Notification;
import android.net.Uri;
import android.os.Bundle;
import android.service.carrier.CarrierMessagingService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Xian on 10/27/2015.
 */
public class MyActivity extends Activity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{


    public static final String TAG = "MyActivity";
    public static final String path = "/wearable_data";

    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_activity);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();


        Button mSendNotificationButton = (Button) findViewById(R.id.send_notification_button);
        mSendNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Notification notification = new NotificationCompat.Builder(getApplication())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Hello World")
                        .setContentText("Hello world")
                        .extend(
                                new  NotificationCompat.WearableExtender().setHintShowBackgroundOnly(true))
                        .build();

                NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplication());

                int notificationId = 1;
                notificationManagerCompat.notify(notificationId, notification);
            }
        });

        final EditText mLocation = (EditText) findViewById(R.id.data_location);
        final EditText mType = (EditText) findViewById(R.id.data_location);
        final EditText mSuugestion = (EditText) findViewById(R.id.data_suggestion);


        final Button mSendButton = (Button) findViewById(R.id.button_send);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = mLocation.getText().toString();
                int type = Integer.parseInt(mType.getText().toString());
                String suggestion = mSuugestion.getText().toString();
                String dataToSend = "";
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("location", location);
                    jsonObject.put("suggestion", suggestion);
                    jsonObject.put("type", type);
                    dataToSend = jsonObject.toString();
                }catch (JSONException e){
                    e.getStackTrace();
                }

                if (dataToSend.length()==0) return;
                DataMap dataMap = new DataMap();
                dataMap.putString("data", dataToSend);
                new SendToDataLayerThread(path,dataMap);
            }
        });
    }

    class SendToDataLayerThread extends Thread{
        String path;
        DataMap dataMap;

        SendToDataLayerThread(String p, DataMap d){
            path = p;
            dataMap = d;
        }

        public void run(){
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
            for (Node node : nodes.getNodes()){
                PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(path);
                putDataMapRequest.getDataMap().putAll(dataMap);
                PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest();
                DataApi.DataItemResult dataItemResult = Wearable.DataApi.putDataItem(mGoogleApiClient,putDataRequest).await();
                if (dataItemResult.getStatus().isSuccess()){
                    Log.d(TAG,"data sent");
                }else{
                    Log.d(TAG,"data not sent");
                }
            }
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnected(Bundle connectionHint){
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onConnected: " + connectionHint);
        }
    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int cause) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onConnectionSuspended: " + cause);
        }
    }

    @Override // GoogleApiClient.OnConnectionFailedListener
    public void onConnectionFailed(ConnectionResult result) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onConnectionFailed: " + result);
        }
    }
}
